const mongoose = require('mongoose');

const Note = mongoose.model('Note', {
    title: {
        type: String,
        required: true,
        minLength: 2,
        trim: true
    },
    text: {
        type: String,
        required: true,
        minLength: 3,
        trim: true
    }
});

module.exports = {
    Note
};