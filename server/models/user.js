const mongoose = require('mongoose');


const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        minLength: 3,
        trim: true,
        unique: true,
    },
    password: {
        type: String,
        require: true,
        minLength: 6
    },
    tokens: [{
        access: {
            type: String,
            require: true
        },
        token: {
            type: String,
            require: true
        }
    }]
});

UserSchema.methods.generateAuthToken = function () {
    const user = this;
    const access = 'auth';
    const token = jwt.sign({_id: user._id.toHextString(), access}, process.env.JWT_SECRET).toString();

    user.tokens = [...user.tokens, token];

    return user.save().then(() => {
        return token;
    })
};

UserSchema.methods.toJSON = function () {
    const user = this;
    const userObject = user.toObject();

    return _.pick(userObject, ['_id', 'username']);
};

const User = mongoose.model('User', UserSchema);

module.exports = {
    User
};