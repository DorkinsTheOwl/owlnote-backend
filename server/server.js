require('./config/config');

const express = require('express');
const bodyparser = require('body-parser');

const { mongoose } = require('./db/mongoose');
const { Note } = require('./models/note');

const app = express();
const port = process.env.PORT;

app.use(bodyparser.json());

app.post('/createNote', async (req, res) => {
    try {
        const note = new Note({
            title: req.body.title,
            text: req.body.text,
        });

        const savedNote = await note.save();
        res.send(savedNote);
    } catch (e) {
        res.status(400).send()
    }
});

app.get('/owlNotes', async (req, res) => {
    try {
        const notes = await Note.find();
        res.send({notes});
    } catch (e) {
        res.status(400).send(e);
    }
});

app.get('/*', (req, res) => {
    res.send(`Placeholder! Hello there!`);
});

app.listen(port, () => {
    console.log(`Started on port ${port}`);
});